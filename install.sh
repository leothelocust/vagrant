#!/usr/bin/env bash

echo "---------------------------------------------------------"
echo "---------------------------------------------------------"
echo "---------------------------------------------------------"
echo "  _   _      _ _         __  __           _"
echo " | | | | ___| | | ___   |  \\/  | __ _ ___| |_ ___ _ __"
echo " | |_| |/ _ \\ | |/ _ \\  | |\\/| |/ _\` / __| __/ _ \\ \`__|"
echo " |  _  |  __/ | | (_) | | |  | | (_| \\__ \\ ||  __/ |"
echo " |_| |_|\\___|_|_|\\___/  |_|  |_|\\__,_|___/\\__\\___|_|"
echo "---------------------------------------------------------"
echo "---------------------------------------------------------"
echo "---------------------------------------------------------"
echo "---------------------------------------------------------"
echo "  ___             _    _          _"
echo " | _ \\_ _ _____ _(_)__(_)___ _ _ (_)_ _  __ _"
echo " |  _/ '_/ _ \\ V / (_-< / _ \\ ' \\| | ' \\/ _\` |"
echo " |_| |_| \\___/\\_/|_/__/_\\___/_||_|_|_||_\\__, |"
echo "                                        |___/"
echo "---------------------------------------------------------"
# Updating packages list
sudo apt-get update

echo "--------------------------------"
echo "--- MySQL Configuration Time ---"
echo "--------------------------------"
echo "--- Updating MySQL password ---"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
echo "--- Done with MySQL Configuration ---"

echo "--------------------------------"
echo "--- Installing Base Packages ---"
echo "--------------------------------"
sudo apt-get install -y vim curl python-software-properties zsh git python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
echo "--- Update packages list ---"
sudo apt-get update

echo "--------------------------------------"
echo "--- Installing Node, MySQL, Apache ---"
echo "--------------------------------------"
sudo apt-get install -y mysql-server-5.5 php5-mysql apache2 nodejs

echo "-------------------------"
echo "--- Set Document Root ---"
echo "-------------------------"
sudo rm -rf /var/www/html
sudo ln -fs /vagrant /var/www/html

echo "-------------------"
echo "--- MySQL Pager ---"
echo "-------------------"
sudo apt-get install -y grc
curl -L -s https://raw.githubusercontent.com/nitso/colour-mysql-console/master/.grcat > /home/vagrant/.grcat
echo "[mysql]
pager  = grcat ~/.grcat | less -RSFXin" > /home/vagrant/.my.cnf

echo "------------------------"
echo "--- Set Some Aliases ---"
echo "------------------------"
curl -L -s https://bitbucket.org/!api/2.0/snippets/leothelocust/zK59e/183adcf88d309307656b8bd1b59d6a75190eabcd/files/aliases-min.sh > /home/vagrant/.aliases

echo "-----------------"
echo "--- Zsh Setup ---"
echo "-----------------"
echo "--- Clone Oh-My-Zsh ---"
git clone git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
echo "--- Copy zshrc template to ~/.zshrc ---"
cp /home/vagrant/.oh-my-zsh/templates/zshrc.zsh-template /home/vagrant/.zshrc
echo "--- Change Default Shell to ZSH for vagrant user ---"
sudo chsh -s /bin/zsh vagrant
echo "--- Start setting up some nice defaults in zshrc ---"
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="avit"/g' /home/vagrant/.zshrc
echo 'export EDITOR="vim"' >> /home/vagrant/.zshrc
echo '. ~/.aliases' >> /home/vagrant/.zshrc
echo 'setopt correct' >> /home/vagrant/.zshrc
echo 'DISABLE_AUTO_TITLE=true' >> /home/vagrant/.zshrc
echo 'PAGER="grcat /Users/lolson/.grcat | less -RSFXin"' >> /home/vagrant/.zshrc
echo 'DISABLE_CORRECTION="true"' >> /home/vagrant/.zshrc
echo 'unsetopt correct_all' >> /home/vagrant/.zshrc
echo 'unsetopt correct' >> /home/vagrant/.zshrc
echo "cd /vagrant" >> /home/vagrant/.zshrc
echo "--- Done setting up some nice defaults in zshrc ---"

echo "  _         _   _           _         _   _     _     _"
echo " | |    ___| |_( )___    __| | ___   | |_| |__ (_)___| |"
echo " | |   / _ \\ __|// __|  / _\` |/ _ \\  | __| '_ \\| / __| |"
echo " | |__|  __/ |_  \\__ \\ | (_| | (_) | | |_| | | | \\__ \\_|"
echo " |_____\\___|\\__| |___/  \\__,_|\\___/   \\__|_| |_|_|___(_)"
